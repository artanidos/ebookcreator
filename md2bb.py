# import markdown2
# import re


# #build godot: E:\Godot\Godot_v4.0-stable_win64.exe --rendering-driver opengl3 --path E:\SourceCode\Ebook --export-release "Windows Desktop" "E:\SourceCode\Ebook\Ebook.exe"

# # Define your Markdown input
# markdown_input = "This is some **bold** text."

# # Convert Markdown to HTML
# html_output = markdown2.markdown(markdown_input)

# # Define a regular expression pattern to match HTML tags
# pattern = re.compile(r'<(.*?)>')

# # Define a dictionary mapping HTML tags to BBCode markup
# html_to_bbcode = {
#     'strong': 'b',
#     'em': 'i',
#     'code': 'code',
#     'a': 'url',
#     'img': 'img',
#     'ul': 'list',
#     'ol': 'list',
#     'li': '*',
#     'blockquote': 'quote',
#     'hr': 'hr',
#     'br': 'br'
# }

# # Define a function to replace HTML tags with BBCode markup
# def replace_tags(match):
#     tag = match.group(1)
#     if tag.startswith('/'):
#         bbcode = f'[/{html_to_bbcode.get(tag[1:], tag[1:])}]'
#     else:
#         bbcode = f'[{html_to_bbcode.get(tag, tag)}]'
#     return bbcode

# # Convert HTML to BBCode using the replace_tags function
# bbcode_output = pattern.sub(replace_tags, html_output)

# # Print the output
# print(bbcode_output)

import re

def parse_markdown_to_bbcode(markdown_text):
    # Regular expressions for matching Markdown elements
    header_pattern = r'^(#+)\s(.+)$'
    list_item_pattern = r'^\*\s(.+)$'
    bold_pattern = r'\*\*(.*?)\*\*'
    italic_pattern = r'\*(.*?)\*'

    parsed_text = ""
    in_list = False

    for line in markdown_text.splitlines():
        # Match headers
        header_match = re.match(header_pattern, line)
        if header_match:
            level = len(header_match.group(1))
            parsed_text += f"[size={7 - level}][b]{header_match.group(2)}[/b][/size]\n"
            continue

        # Match list items
        list_item_match = re.match(list_item_pattern, line)
        if list_item_match:
            if not in_list:
                parsed_text += "[list]\n"
                in_list = True
            parsed_text += f"[*]{list_item_match.group(1)}\n"
            continue

        if in_list:
            parsed_text += "[/list]\n"
            in_list = False

        # Match bold and italic
        line = re.sub(bold_pattern, r'[b]\1[/b]', line)
        line = re.sub(italic_pattern, r'[i]\1[/i]', line)

        parsed_text += line + "\n"

    if in_list:
        parsed_text += "[/list]\n"

    return parsed_text

if __name__ == "__main__":
    markdown_text = """
# This is a Header
## Subheader
* List item 1
* List item 2
**This is bold text** and *this is italic text*.
    """
    parsed = parse_markdown_to_bbcode(markdown_text)
    print(parsed)

